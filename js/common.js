// IE10以下浏览器提示
function hiUpgrade() {
    window.AESKey = '';
    // 判断浏览器是否支持placeholder属性
    function isSupportPlaceholder() {
        var input = document.createElement('input');
        return 'placeholder' in input;
    };
    //判断是否是IE浏览器，包括Edge浏览器
    function IEVersion() {
        //取得浏览器的userAgent字符串
        var userAgent = navigator.userAgent;
        //判断是否IE浏览器
        var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1;
        if (isIE) {
            // ie10及以下
            var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
            reIE.test(userAgent);
            var fIEVersion = parseFloat(RegExp["$1"]);
            if (fIEVersion < 10 || !isSupportPlaceholder()) {
                return true;
            }
        } else {
            return false;
        }
    }
    var tpl = '<div id="hi-upgrade"><div class="hi-wrap"><p class="hi-title">无法正常浏览本网站！</p><div class="hi-close">继续浏览</div><div class="hi-text1"><p>1、您的浏览器版本过低，请升级您的浏览器。</p><p>2、如果您的浏览器是最新版本，请<span>切换到极速模式</span>访问。</p><p>3、您使用的是IE10以下的浏览器，建议您<span>使用主流浏览器</span>访问。</p></div><p class="hi-text2"><span>主流浏览器下载</span></p><ul class="hi-list"><li><a href="https://www.google.cn/intl/zh-CN/chrome/" target="_blank"><div class="hi-ico1"></div><p>谷歌浏览器</p></a></li><li><a href="http://www.firefox.com.cn/download/" target="_blank"><div class="hi-ico2"></div><p>火狐浏览器</p></a></li><li><a href="http://browser.360.cn" target="_blank"><div class="hi-ico3"></div><p>UC浏览器</p></a></li><li><a href="https://www.uc.cn" target="_blank"><div class="hi-ico4"></div><p>360浏览器</p></a></li><li><a href="https://browser.qq.com" target="_blank"><div class="hi-ico5"></div><p>QQ浏览器</p></a></li><li><a href="https://ie.sogou.com" target="_blank"><div class="hi-ico6"></div><p>搜狗浏览器</p></a></li></ul></div></div>';
    if (IEVersion()) {
        document.write(tpl);
    }
}
hiUpgrade();

// 阻止浏览器橡皮筋效果
document.addEventListener('touchmove', function(e) {
    e.preventDefault(); //阻止默认的处理方式(阻止下拉滑动的效果)
}, { passive: false }); //passive 参数不能省略，用来兼容ios和android

// 阻止body冒泡
document.body.addEventListener('touchmove', function(e) {
    e.stopPropagation();
});


// 等高
$(document).ready(function() {
    $('.c-dg').matchHeight();
    $("#c-anp1").addClass("on");
});

// 导航
function cNav() {
    var oBody = $("body");
    var oHead = $("#c-header");
    var oNav = $("#c-header .c-nav");
    var oBtn = $("#c-header .c-switch");
    var oL = $("#c-header .c-nav>li");
    var oTitle = $("#c-header .c-nav2 li .c-title-box");
    var num = 0;
    var i = 0;
    var oP = $("#c-placeholder");
    var b = true;
    var t = null;

    // 窗口重置隐藏手机端导航
    $(window).resize(function() {
        console.log(11)
        if ($(window).width()>991) {
            oBody.removeClass('c-open');
        }
    });

    // 手机端导航栏目下拉
    oTitle.click(function() {
        $(this).next().stop().slideToggle();
    });

    // 鼠标移入导航样式
    oHead.hover(function() {
        $(this).addClass("c-style2");
    }, function() {
        if ($(window).scrollTop() <= oHead.outerHeight() && oP.length == 0 && !oBody.hasClass("c-open")) {
            oHead.removeClass("c-style2");
        } else if ($(window).scrollTop() > oHead.outerHeight()) {
            oHead.addClass("c-style2");
        } else if ($(window).scrollTop() <= oHead.outerHeight() && oP.length != 0) {
            oHead.addClass("c-style2");
        }
    });

    // 手机端导航显示
    oBtn.click(function() {
        if (b) {
            b = false;
            // t = $(window).scrollTop();
            oBody.addClass('c-open');
            // oBody.css("top", -t);
        } else {
            b = true;
            oBody.removeClass('c-open');
            // oBody.css("top", "0");
            // $(window).scrollTop(t);
        }
    });

    // 导航显示及样式
    function fn1() {
        if ($(window).scrollTop() - i > 0 && $(window).scrollTop() > oHead.outerHeight() && !oBody.hasClass("c-open")) {
            i = $(window).scrollTop();
            oHead.addClass("c-head-move");
            oHead.addClass("c-style2");
        } else if ($(window).scrollTop() - i <= 0) {
            i = $(window).scrollTop();
            oHead.removeClass("c-head-move");
            if ($(window).scrollTop() <= oHead.outerHeight() && oP.length == 0 && !oBody.hasClass("c-open")) {
                oHead.removeClass("c-style2");
            } else if ($(window).scrollTop() > oHead.outerHeight()) {
                oHead.addClass("c-style2");
            } else if ($(window).scrollTop() <= oHead.outerHeight() && oP.length != 0) {
                oHead.addClass("c-style2");
            }
        }
    }
    fn1();
    $(window).scroll(function() {
        fn1();
    });

    // pc导航动画
    oL.each(function() {
        if ($(this).hasClass("on")) {
            num = $(this).index();
        }
        $(this).hover(function() {
                oL.eq(num).removeClass("on");
                $(this).children("ul").stop().slideDown();
            },
            function() {
                oL.eq(num).addClass("on");
                $(this).children("ul").stop().slideUp();
            });
    });
}
cNav();

// 顶部搜索
function topForm() {
    var obj = $("#c-header form");
    var oBox = obj.find(".c-box");
    obj.hover(function() {
        oBox.stop().fadeIn();
    }, function() {
        oBox.stop().fadeOut();
    });
}
topForm();

// 首页轮播
function banner1() {
    var obj = $(".c-banner1");
    var swiper = new Swiper(obj, {
        // effect : 'fade',
        loop: true,
        speed: 1000,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        // breakpoints: {
        //     768: {
        //         slidesPerView: 3,
        //         spaceBetween: 20,
        //     }
        // }
    });
    // obj.hover(function() {
    //     swiper.autoplay.stop();
    // }, function() {
    //     swiper.autoplay.start();
    // });
}
banner1();

// anp公共轮播
function anp(obj,aLi) {
    aLi.eq(0).addClass("on");
    var swiper = new Swiper(obj, {
        // effect : 'fade',
        speed: 1000,
        // fadeEffect: {
        //     crossFade: true,
        // },
        on:{
            slideChange: function(){
              aLi.eq(this.realIndex).addClass("on").siblings().removeClass("on");
            },
        },
    });
    aLi.hover(function(){
        swiper.slideTo($(this).index(), 1000, false);
        $(this).addClass("on").siblings().removeClass("on");
    });
}
anp($(".c-anp3-banner"),$("#c-anp3 .c-list li"));
anp($(".c-anp11-banner"),$("#c-anp11 .c-list li"));

// anp第5\6\7屏轮播
$(".c-anp-common").each(function(){
    var aVideo = $(this).find(".c-anp-common-banner-right .swiper-slide");
    aVideo.eq(0).find("video").trigger("play");
    var thumbsSwiper = new Swiper($(this).find(".c-anp-common-banner-left"),{
        direction : 'vertical',
        slidesPerView: "auto",
        watchSlidesVisibility: true,//防止不可点击
    })
    var gallerySwiper = new Swiper($(this).find(".c-anp-common-banner-right"),{
        speed: 1000,
        thumbs: {
            swiper: thumbsSwiper,
        },
        on:{
            slideChange: function(){
                console.log(1)
                aVideo.eq(this.realIndex).find("video")[0].currentTime = 0;
                aVideo.eq(this.realIndex).find("video").trigger("play");
                aVideo.eq(this.realIndex).siblings().find("video").trigger("pause");
            },
        },
    })
    $(this).find(".c-anp-common-banner-left .swiper-slide").hover(function(){
        gallerySwiper.slideTo($(this).index(), 1000, false);
    });
});

// anp9轮播
function anp9(obj,aLi) {
    aLi.eq(0).addClass("on");
    var obj2 = $("#c-anp9");
    var oT = obj2.offset().top;
    var swiper = new Swiper(obj, {
        // effect : 'fade',
        speed: 1000,
        // fadeEffect: {
        //     crossFade: true,
        // },
        on:{
            slideChange: function(){
              aLi.eq(this.realIndex).addClass("on").siblings().removeClass("on");
              if(this.realIndex == 0){
                obj2.addClass("on1").removeClass("on2").removeClass("on3");
              }
              if(this.realIndex == 1){
                obj2.addClass("on2").removeClass("on1").removeClass("on3");
              }
              if(this.realIndex == 2){
                obj2.addClass("on3").removeClass("on1").removeClass("on2");
              }
            },
        },
    });
    aLi.hover(function(){
        swiper.slideTo($(this).index(), 1000, false);
        $(this).addClass("on").siblings().removeClass("on");
    });

    function fn1(){
        if($(window).scrollTop() > oT){
            obj2.addClass("active");
        }
    }
    fn1();
    $(window).scroll(function(){
        fn1();
    });
    $(window).resize(function(){
        oT = obj2.offset().top;
    });
}
$(document).ready(function(){
    anp9($(".c-anp9-banner"),$("#c-anp9 .c-list li"));
});


// anp第12屏视频播放
function anp12(){
    var aVideo = $(".c-anp12-banner .swiper-slide");
    var swiper = null;

    function fn1(){
        console.log($(window).width())
        if($(window).width()>970){
            aVideo.eq(0).addClass("on");
            aVideo.eq(0).find("video").trigger("play");
            aVideo.mouseenter(function(){
                $(this).addClass("on").siblings().removeClass("on");
                $(this).find("video")[0].currentTime = 0;
                $(this).find("video").trigger("play");
                $(this).siblings().find("video").trigger("pause");
            });
        }else{
            swiper = new Swiper(".c-anp12-banner", {
                speed: 1000,
                breakpoints: {
                    767: {
                        slidesPerView: 1,
                        spaceBetween: 20,
                    },
                    991: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                }
            });
        }
    }

    fn1();
    $(window).resize(function(){
        fn1()
    });
}
anp12();

// 可视化数据滚动
function visualData(obj) {
    $(window).load(function() {
        obj.each(function() {
            var h = Number($(this).html());
            var t = "";
            var n = Math.ceil(h / 20);
            var a = true;
            var This = $(this);
            if ($(this).length != 0) {
                t = $(this).offset().top;
            }
            This.html(0);
            fn1();
            $(window).scroll(function() {
                fn1();
            });

            function fn1() {
                var wT = $(window).scrollTop();
                if (wT > t - $(window).height() + 50 && wT < t - 50 && a == true) {
                    a = false;
                    var y = 0;
                    var timer2 = setInterval(function() {
                        if (y >= h) {
                            y = h;
                            clearInterval(timer2);
                        }
                        This.html(y);
                        y += n;
                    }, 100);
                }
            }
        });
    });
}
visualData($(".c-num-move"));


// 侧边栏回到顶部
function goTop() {
    var obj = $("#c-go-top");
    var oBtn = $("#c-go-top");
    oBtn.click(function() {
        $("html,body").animate({ scrollTop: 0 }, 500);
    });

    function fn1() {
        if ($(window).scrollTop() > $(window).height()) {
            obj.fadeIn();
        } else {
            obj.fadeOut();
        }
    }
    fn1();
    $(window).scroll(function() {
        fn1();
    });
}
goTop();

// 底部导航
function footerNav() {
    var aList = $("#c-footer .c-list-box");
    if ($(window).width() <= 767) {
        aList.each(function() {
            var This = $(this);
            $(this).find(".c-title-box").click(function() {
                This.toggleClass("on");
                This.find(".c-list").stop().slideToggle();
            });
        });
    }
}
footerNav();

// 点击底部微信弹出二维码
function weixin() {
    var b = $("#c-footer .c-wx-btn");
    var w = $("#c-footer .c-weixin");
    var c = $("#c-footer .c-weixin .c-close");
    var d = w.find(".c-img-box");
    b.click(function() {
        w.stop().fadeToggle();
    });
    d.click(function() {
        return false;
    });
    w.click(function() {
        w.stop().fadeToggle();
    });
    c.click(function() {
        w.stop().fadeToggle();
    });
}
weixin();


// 页面进入动画
(function($) {
    function wow() {
        var oT = $(window).height()*0.1;
        var t = .1,
            e = !0,
            wow = new WOW({
                boxClass: "wow",
                animateClass: "animated",
                offset: oT,
                callback: function(e) {
                    (e = $(e)).css("animation-delay", t.toFixed(1) + "s"),
                        t += .1;
                }
            });
        $(window).scroll(function() {
                e && (t = .2, setTimeout(function() {
                    e = !0
                }, 150), e = !1)
            }),
            wow.init();
    }
    wow();
}(jQuery));

